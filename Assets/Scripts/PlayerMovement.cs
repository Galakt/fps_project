﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	private CharacterController controller;
	private Vector2 axis;
    private bool jump;
    private float forceToGround = Physics.gravity.y;
    private Animator anim;

    public float speed;
    public Vector3 moveDirection;
    public float jumpSpeed;
	public float gravityMagnitude = 1.0f;

	void Start ()
	{
		controller = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

	void Update ()
	{
		if(controller.isGrounded && !jump)
		{
			moveDirection.y = forceToGround;
		}
		else
		{
			jump = false;
			moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
		}

		Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

		moveDirection.x = transformDirection.x * speed;
		moveDirection.z = transformDirection.z * speed;

		controller.Move(moveDirection * Time.deltaTime);

	}

	public void SetAxis(Vector2 inputAxis)
	{
		axis = inputAxis;
	}

	public void StartJump()
	{
		if(!controller.isGrounded) return;

		moveDirection.y = jumpSpeed;
		jump = true;
	}

    void PlayerWalk()
    {
        if(moveDirection.x  || moveDirection.y)
        {
            animation.SetBool("Walking", false);
            anim.Play("Idle");
        }
        else
        {
            animation.SetBool("Walking", true);
            anim.Play("Walk");
        }
    }

    void PlayerJump()
    {
        animation.SetBool("Grounded", false);
    }
}
